//  Copyright © 2018-2019 Brian E. Coggins. All rights reserved.

#ifndef BEC_ZIP_INCLUDED
#define BEC_ZIP_INCLUDED

#include <vector>
#include <tuple>
#include <optional>
#include <utility>
#include <memory>
#include <type_traits>
#include <numeric>

/// Tools for iterating over multiple containers simultaneously using conventional and range-for loops
namespace bec::zip_for
{
    namespace details
    {
        /// Deduces the type of a container's iterator class.
        template< typename T >
        struct container_iter_type
        {
            using type = decltype( std::declval< T >().begin() );
        };
        
        /// The type of the supplied container type's iterator class.
        template< typename T >
        using container_iter_type_t = typename container_iter_type< T >::type;
        
        /// Deduces the type returned from the dereferencing of container's iterator type.
        template< typename T >
        struct container_iter_deref_type
        {
            using type = decltype( *( std::declval< T >().begin() ) );
        };
        
        /// The type returned from the dereferencing of a container's iterator type.
        template< typename T >
        using container_iter_deref_type_t = typename container_iter_deref_type< T >::type;
        
        /// Deduces the type returned from the dereferencing of an iterator of type T
        template< typename T >
        struct iter_deref_type
        {
            using type = decltype( *( std::declval< T >() ) );
        };
        
        /// The type returned from the dereferencing of an iterator of the supplied type.
        template< typename T >
        using iter_deref_type_t = typename iter_deref_type< T >::type;
        
        /// Dereference all iterators within a tuple of iterators, and return a tuple of the results (typically a tuple of references to container elements).
        template< typename ...Ts >
        auto deref_iter_tuple( std::tuple< Ts... > & iter_tuple )
        {
            using tuple_type = typename std::tuple< iter_deref_type_t< Ts >... >;
            auto tuple_from_deref_of_arg_list = [](auto&... args){ return tuple_type{ *( args )... }; };
            return std::apply( tuple_from_deref_of_arg_list, iter_tuple );
        }
        
        /// Increment all iterators within a tuple of iterators.
        template< typename ...Ts >
        void incr_iter_tuple( std::tuple< Ts... > & iter_tuple )
        {
            auto incr_each_arg = [](auto &... args){ ( ++( args ), ... ); };
            std::apply( incr_each_arg, iter_tuple );
        }
        
        /**
         Compare the element at some position in one tuple to the element at the same position in another tuple, and report if they are not equal.
         
         The two tuples are supplied as arguments, and the index of the position to test is provided as a template parameter.  This function is designed to be used as part of a fold expression over all elements of a tuple.
         */
        template< typename A, typename B, std::size_t I >
        bool comp_neq_elems( const A & a, const B & b )
        {
            return std::get< I >( a ) != std::get< I >( b );
        }
        
        /**
         Compare pairs of corresponding elements between two tuples, and report whether the all of the pairs test as not equal (implementation).
         
         Each pair is tested by passing the two tuples to a support function, along with an index stating which elements to compare.  The index is drawn from the supplied index sequence.  The full comparison is achieved by a fold expression across the integer sequence.
         */
        template< typename A, typename B, std::size_t...Is >
        bool comp_neq_tuple_elements_by_index( const A & a, const B & b, std::index_sequence< Is... > iseq )
        {
            return ( comp_neq_elems< A, B, Is >( a, b ) && ... );
        }
        
        /**
         Compare pairs of corresponding elements between two tuples, and report whether the all of the pairs test as not equal.
         
         The actual testing must be delegated to an implementation function so that a static integer sequence can be supplied as a template parameter pack.
         */
        template< typename A, typename B >
        bool comp_neq_tuple_elements( const A & a, const B & b )
        {
            return comp_neq_tuple_elements_by_index( a, b, std::make_index_sequence< std::tuple_size_v< A > >{} );
        }

        /**
         Makes a copy of an argument which may have been supplied by reference.
         
         The template accepts any kind of argument: by value, by reference, by rvalue reference, with or without const qualification.  All of these match T.  Regardless of how the argument is supplied, `remove_reference_t` obtains a typename for the corresponding value type, and instantiation of a new instance of that value type will cause a copy to be made.
         */
        template< typename T >
        auto make_copy_of_elem( T arg )
        {
            return std::remove_reference_t< T >{ arg };
        }
        
        /**
         Accept a tuple of references and create a new tuple containing copies of the referenced values (implementation).
         
         To make a tuple containing copies of referenced tuple element, each element is retrieved from the tuple of references and passed to a function which makes a copy.  This is achieved by folding over the tuple elements using the integer sequence provided as a template parameter pack.  A variadic template template parameter pack is needed for the compiler to deduce the element types in the reference tuple.
         */
        template< typename ...RefTupleTs, template< typename... > class RefTupleType, std::size_t ...Is >
        auto make_value_tuple_impl( const RefTupleType< RefTupleTs... > & orig_ref_tuple, std::index_sequence< Is... > iseq )
        {
            return std::make_tuple( make_copy_of_elem( std::get< Is >( orig_ref_tuple ) )... );
        }
        
        /**
         Accept a tuple of references and create a new tuple containing copies of the referenced values.
         
         The actual generation of the tuple must be delegated to an implementation function so that a static integer sequence can be supplied as a template parameter pack.
         */
        template< typename RefTupleType >
        auto make_value_tuple( const RefTupleType & orig_ref_tuple )
        {
            return make_value_tuple_impl( orig_ref_tuple, std::make_index_sequence< std::tuple_size_v< RefTupleType > >{} );
        }
        
        /**
         Make a tuple of references referring to the elements of the supplied tuple of values (implementation)
         
         To make a tuple of references, each element is retrieved from the tuple of values and static_cast is used to add the reference to the type.  This is achieved by folding over the tuple elements using the integer sequence provided as a template parameter pack.  Variadic template template parameter packs are needed for the compiler to deduce the element types in the reference and value tuples, so that folding over these types can be done.  The `orig_ref_tuple` should be an example of the desired reference tuple type; it has no purpose except to allow the compiler to deduce the template template parameter pack containing its element reference types.
         */
        template< typename ...RefTupleTs, template< typename... > class RefTupleType, typename ...ValueTupleTs, template< typename... > class ValueTupleType, std::size_t ...Is >
        auto make_ref_tuple_impl( const RefTupleType< RefTupleTs... > & orig_ref_tuple, ValueTupleType< ValueTupleTs... > & value_tuple, std::index_sequence< Is... > iseq )
        {
            return std::tuple< RefTupleTs... >{ static_cast< RefTupleTs >( std::get< Is >( value_tuple ) )... };
        }
 
        /**
         Make a tuple of references referring to the elements of the supplied tuple of values.
         
         The actual generation of the tuple must be delegated to an implementation function so that a static integer sequence can be supplied as a template parameter pack.
         
         @param orig_ref_tuple Any instance of the desired reference tuple type.  The contents of this tuple aren't used for anything, but an example of the tuple type is needed for the compiler to deduce the correct types.
         @param value_tuple The tuple of values to which the generated reference tuple should refer.
         */
        template< typename RefTupleType, typename ValueTupleType >
        auto make_ref_tuple( const RefTupleType & orig_ref_tuple, ValueTupleType & value_tuple )
        {
            return make_ref_tuple_impl( orig_ref_tuple, value_tuple, std::make_index_sequence< std::tuple_size_v< RefTupleType > >{} );
        }

        /// Wrapper class used to provide proper reference and value semantics for structured bindings in range-for loops over multiple containers.
        template< typename RefTupleType, typename ValueTupleType >
        class tuple_return_wrapper
        {
        public:
            tuple_return_wrapper() = delete;
            explicit tuple_return_wrapper( RefTupleType orig_tuple ) : value_tuple_opt{}, ref_tuple{ orig_tuple } {  }
            tuple_return_wrapper( const tuple_return_wrapper & copy_from ) : value_tuple_opt{ make_value_tuple( copy_from.ref_tuple ) }, ref_tuple{ make_ref_tuple( copy_from.ref_tuple, *value_tuple_opt ) } {  }
            tuple_return_wrapper & operator=( const tuple_return_wrapper & copy_from_rhs ) = delete;
            tuple_return_wrapper( tuple_return_wrapper && move_from ) noexcept = delete;
            tuple_return_wrapper & operator=( tuple_return_wrapper && move_from_rhs ) noexcept = delete;
            ~tuple_return_wrapper() = default;
            
            /// Returns an element from the underlying stored tuple of references.
            template< std::size_t I >
            decltype( auto ) get() const
            {
                return std::get< I >( ref_tuple );
            }
            
        private:
            /// When needed (when the structured binding wants copies), stores a tuple of copies of the referenced container elements.  These are returned by reference to the structured binding.
            std::optional< ValueTupleType > value_tuple_opt;
            
            /// Stores a tuple of references to container elements, either (1) to the original container elements or (2) if copies were requested, to the copies made and stored within the `value_tuple_opt`.
            RefTupleType ref_tuple;
        };
        
        /**
         Adaptor class supplied to a range-for loop to allow simultaneous iteration over multiple containers.
         
         This class implements the interface of a range, but does not contain any data elements.  Instead, it holds references to the containers over which to iterate, and it returns iterators allowing simultaneous iteration and simultaneous access to corresponding data elements in the containers.
         
         To maintain proper reference and value semantics in range-for structured bindings accessing the elements of the containers, a wrapper class is used.  A tuple of container element references is stored inside an instance of the wrapper class.  This is constructed in a std::optional within the iterator class layout, to avoid repeated heap allocation operations.  This wrapper must detect whether it is being received by reference or by value.  If the structured binding receives it by reference, no copy is made, and the tuple of references is used in the structured binding to provide by-reference access to the original container elements.  If the structured binding receives the wrapper by value, the copy constructor is invoked.  This copy constructor makes copies of the referenced container elements, and then provides the structured binding with a tuple of references to the copies.  These copies will be valid for the duration of the wrapper, which is equal to the duration of that particular iteration.
         
         (Why not simply let the structured binding make copies?  It can't, because of the semantics of tuples.  A copy of a tuple of references is also a tuple of references, referring back to the original entities.  Thus if we return a tuple of references to the structured binding, the structured binding will either use that tuple or make a copy of that tuple, but either way it gets references.  The wrapper class, however, can detect when the structured binding mechanism makes a copy, and at that instant it breaks the references back to the original elements.  Why does the wrapper return a tuple of references to its copies, and not the copies themselves?  Because the return type of a function must always be the same.  Thus the wrapper always returns a tuple of references, either to the original elements or to copies of the elements.)
         */
        template< typename ...Containers >
        class zip_adaptor
        {
        public:
            class iterator
            {
            public:
                using ref_tuple_type = typename std::tuple< container_iter_deref_type_t< Containers >... >;                                     /// Tuple type for tuples containing elements directly received from the containers.  Preserve references when these are references (normally the case).
                using value_tuple_type = typename std::tuple< std::remove_reference_t< container_iter_deref_type_t< Containers > >... >;        /// Tuple type for tuples containing copies of elements made from the elements in the containers.  Remove references and hold only values.
                using wrapped_value_type = typename bec::zip_for::details::tuple_return_wrapper< ref_tuple_type, value_tuple_type >;            /// Type of the wrapper class to be returned when the iterator is dereferenced.
                
                template< typename T >
                iterator( T iter_tuple ) : iters{ iter_tuple }, deref_tuple_opt{}  { }
                
                wrapped_value_type & operator*()
                {
                    //  Construct the wrapper in-place in the std::optional.  Return this by reference.  If the structured binding makes a copy of the wrapper, the wrapper instance will copy the values and break the references back to the original elements.
                    deref_tuple_opt.emplace( deref_iter_tuple( iters ) );
                    return *deref_tuple_opt;
                }
                
                iterator & operator++()
                {
                    incr_iter_tuple( iters );
                    return *this;
                }
                
                iterator operator++( int )
                {
                    iterator i = *this;
                    incr_iter_tuple( iters );
                    return i;
                }
                
                bool operator!=( const iterator & rhs )
                {
                    return comp_neq_tuple_elements( iters, rhs.iters );
                }
                
            private:
                using internal_iter_type = typename std::tuple< container_iter_type_t< Containers >... >;

                /// Tuple of iterators marking the current position in each container.
                internal_iter_type iters;
                
                /// Holds the instance of the wrapper class that is returned (by reference) on iterator dereference.
                std::optional< wrapped_value_type > deref_tuple_opt;
            };
            
            iterator begin()
            {
                auto build_iter_tuple = []( auto&... cs ){ return std::make_tuple( std::begin( cs )... ); };
                return iterator( std::apply( build_iter_tuple, containers ) );
            }
            
            iterator end()
            {
                auto build_iter_tuple = []( auto&... cs ){ return std::make_tuple( std::end( cs )... ); };
                return iterator( std::apply( build_iter_tuple, containers ) );
            }

            zip_adaptor( Containers&&... args ) : containers{ args... }  { }

            
        private:
            std::tuple< Containers... > containers;
        };
        
        /**
         Adaptor class supplied to the `zip_adaptor` to provide the functionality of a counter that increments during the iteration.
         
         This class implements the interface of a range, but does not contain any data elements.  Instead, its iterator holds a counter variable, which is incremented each time the loop advances.  Deferencing the iterator returns the current count.
         */
        class zip_counter_container
        {
        public:
            class iterator
            {
            public:
                int operator*() const
                {
                    return count;
                }
                
                iterator & operator++()
                {
                    count++;
                    return *this;
                }
                
                iterator operator++( int )
                {
                    iterator i = *this;
                    count++;
                    return i;
                }
                
                /// Inequality test; in fact, this always returns `true` since equality tests are meaningless for this type of iterator.
                bool operator!=( const iterator & rhs ) const
                {
                    return true;
                }
                
            private:
                int count = 0;
            };
            
            iterator begin() const
            {
                return iterator{};
            }
            
            iterator end() const
            {
                return iterator{};
            }
        };
        
        /**
         Adaptor class supplied to the `zip_adaptor` to provide the functionality of signaling the last element, when iterating over multiple containers.
         
         This class implements the interface of a range, but does not contain any data elements.  Instead, it holds tuples of iterators to other containers, which its iterator uses to determine when the last element in the multiple-container simultaneous iteration has been reached.  Its iterator determines this by advancing the internal tuple of external iterators one iteration beyond where the rest of the `zip_adaptor` is, and checking these against its stored tuple of end iterators for those containers.  Deferencing the container's iterator returns a signal of `true` if the last iteration has been reached.
         */
        template< typename IterType >
        class zip_signal_last_elem_container
        {
        public:
            class iterator
            {
            public:
                iterator( IterType ci, IterType ei ) : current_iters{ ci }, end_iters{ ei }
                {
                    incr_iter_tuple( current_iters );
                }
                
                /// Dereference the iterator, returning `true` if the multiple-container iteration is on the last iteration, `false` if there are more iterations to follow.
                bool operator*() const
                {
                    if( comp_neq_tuple_elements( current_iters, end_iters ) ) return false;
                    else return true;
                }
                
                iterator & operator++()
                {
                    incr_iter_tuple( current_iters );
                    return *this;
                }
                
                iterator operator++( int )
                {
                    iterator i = *this;
                    incr_iter_tuple( current_iters );
                    return i;
                }

                /// Inequality test; in fact, this always returns `true` since equality tests are meaningless for this type of iterator.
                bool operator!=( const iterator & rhs ) const
                {
                    return true;
                }
                
            private:
                IterType current_iters;
                IterType end_iters;
            };
            
            iterator begin()
            {
                return iterator{ begin_iters, end_iters };
            }
            
            /// Returns the end iterator--which, however, does nothing, since this container should never stop the parallel iteration, but rather should continue as long as the other containers have elements over which to iterate.
            iterator end()
            {
                return iterator{ begin_iters, end_iters };
            }

            zip_signal_last_elem_container( IterType bi, IterType ei ) : begin_iters{ bi }, end_iters{ ei } {  }
            
        private:
            IterType begin_iters;
            IterType end_iters;
        };
        
        /**
         Adaptor class allowing a range of elements from some container to be supplied to a range-for loop.
         */
        template< typename IterType >
        class range_container
        {
        public:
            range_container( IterType begin_iterator, IterType end_iterator ) : begin_iter{ begin_iterator }, end_iter{ end_iterator } {  }
            
            IterType begin()
            {
                return begin_iter;
            }
            
            IterType end()
            {
                return end_iter;
            }
            
        private:
            IterType begin_iter;
            IterType end_iter;
        };
        
    }
    
    /**
     Simultaneously iterate over multiple containers in a range-based for loop.
     
     Call this function after the colon of a range-based for loop, passing in the containers as arguments and using a structured binding to receive the iterated elements.  For example, for two containers `a` and `b`, use `for( auto & [ a_elem, b_elem ] : zip( a, b ) )` to iterate over containers `a` and `b` simultaneously, receiving each pair of elements in the variables `elem_a` and `elem_b`.  Receiving the elements with `auto &` gives references to the elements in the containers, while `auto` alone makes copies of them.

     @param args Containers over which to iterate.  Accepts any container or other object with `begin()` and `end()` members or with `std::begin()` and `std::end()` specializations.  Accepts both lvalues and rvalues, but cannot accept braced-init-lists due to the inability of the compiler to deduce a type for the elements.
     @return An instance of a class implementing the required methods to facilitate a range-for loop over the set of containers.  Note that the return value of this function is not intended to be used directly by the user.
     */
    template< typename ...Ts >
    details::zip_adaptor< Ts... > zip( Ts&&... args )
    {
        return details::zip_adaptor< Ts... >{ std::forward< Ts >( args )... };
    }
    
    /**
     Iterate over one or more containers in a range-based for loop, and simultaneously increment a counter.
     
     Call this function after the colon of a range-based for loop, passing in the containers as arguments and using a structured binding to receive the iterated elements.  Include an extra variable at the end of the structured binding, which will receive the value of the counter.  The counter begins at zero and is incremented after each iteration of the loop.

     @param args One or more containers over which to iterate.  Accepts any container or other object with `begin()` and `end()` members or with `std::begin()` and `std::end()` specializations.  Accepts both lvalues and rvalues, but cannot accept braced-init-lists due to the inability of the compiler to deduce a type for the elements.
     @return An instance of a class implementing the required methods to facilitate a range-for loop over the set of containers.  Note that the return value of this function is not intended to be used directly by the user.
     */
    template< typename ...Ts >
    auto zip_and_counter( Ts&&... args )
    {
        return details::zip_adaptor< Ts..., details::zip_counter_container >{ std::forward< Ts >( args )..., details::zip_counter_container{} };
    }
    
    /**
     Iterate over one or more containers in a range-based for loop, and signal when in the last iteration.
     
     Call this function after the colon of a range-based for loop, passing in the containers as arguments and using a structured binding to receive the iterated elements.  Include an extra variable at the end of the structured binding, which will receive a bool value indicating if the current iteration is the last iteration of the loop.
     
     @param args One or more containers over which to iterate.  Accepts any container or other object with `begin()` and `end()` members or with `std::begin()` and `std::end()` specializations.  Accepts both lvalues and rvalues, but cannot accept braced-init-lists due to the inability of the compiler to deduce a type for the elements.
     @return An instance of a class implementing the required methods to facilitate a range-for loop over the set of containers.  Note that the return value of this function is not intended to be used directly by the user.
     */
    template< typename ...Ts >
    auto zip_and_signal_last_elem( Ts&&... args )
    {
        using slec_iter_type = typename std::tuple< details::container_iter_type_t< Ts >... >;
        using slec_type = typename details::zip_signal_last_elem_container< slec_iter_type >;
        
        auto slec = slec_type( slec_iter_type{ std::begin( args )... }, slec_iter_type{ std::end( args )... } );
        return details::zip_adaptor< Ts..., slec_type >{ std::forward< Ts >( args )..., std::move( slec ) };
    }

    /**
     For simultaneous iteration over multiple containers in a single conventional for loop, obtains a set of beginning iterators for the containers.
     
     Call this function in the initializer section of the for loop to obtain a set of iterators that can be used for simultaneous iteration.
     
     @remark    The `zinit()` function will be a more useful choice for initialization of a for loop in most cases.

     @param args Containers over which to iterate.  Accepts any container or other object with a `begin()` member or with a `std::begin()` specialization.  Containers must be lvalues, since an rvalue's lifetime cannot be extended by this function and will expire before the start of the for loop.
     @return A tuple of iterators.  Receive these using a structured binding, accepting the elements by value (`auto`) rather than reference (`auto &`).
     */
    template< typename ...Ts >
    auto zbegin( Ts&&... args )
    {
        return std::make_tuple( std::begin( args )... );
    }

    /**
     For simultaneous iteration over multiple containers in a single conventional for loop, obtains a set of ending iterators for the containers.
     
     @param args Containers for which to obtain the iterators.  Accepts any container or other object with a `end()` member or with a `std::end()` specialization.  Containers must be lvalues, since an rvalue's lifetime cannot be extended by this function and will expire before the start of the for loop.
     @return A tuple of iterators.  Receive these using a structured binding, accepting the elements by value (`auto`) rather than reference (`auto &`).
     */
    template< typename ...Ts >
    auto zend( Ts&... args )
    {
        return std::make_tuple( std::end( args )... );
    }
    
    /**
     For simultaneous iteration over multiple containers in a single conventional for loop, initialize the loop, obtaining a set of all iterators needed.
     
     This function is designed to facilitate the initailization of a conventional for loop over multiple containers, obtaining--as part of one statement--all iterators needed for managing the loop.  It is designed to go in the initializer section of the for loop.  Receive the iterators by structured binding, putting one variable for each container, each of which will hold an individual iterator referencing the current position, and one additional variable at the end to hold a tuple of end iterators.  Use the individual iterators to access the current element in each container.  In the middle section of the for statement, use `ztest` to check whether the loop should continue.  In the last section of the for loop, increment the current-position iterators.
     
     @param args Containers over which to iterate.  Accepts any container or other object with `begin()` and `end()` members or with `std::begin()` and `std::end()` specializations.  Containers must be lvalues, since an rvalue's lifetime cannot be extended by this function and will expire before the start of the for loop.
     @return A tuple containing one iterator per container and an additional nested tuple containing all of the end iterators.  Receive these using a structured binding, accepting the elements by value (`auto`) rather than reference (`auto &`).
     */
    template< typename ...Ts >
    auto zinit( Ts&... args )
    {
        return std::make_tuple( std::begin( args )..., zend( args... ) );
    }
    
    /**
     For simultaneous iteration over multiple containers in a single conventional for loop, initialize the loop, obtaining a set of all iterators needed (receiving end iterators in separate bindings).
     
     This function is designed to facilitate the initailization of a conventional for loop over multiple containers, obtaining--as part of one statement--all iterators needed for managing the loop.  It is designed to go in the initializer section of the for loop.  Receive the iterators by structured binding, putting one variable for each container's current iterator and then one variable for each container's end iterator, for a total number of variables equal to two times the number of containers.  Use the individual iterators to access the current element in each container.  In the middle section of the for statement, use `ztest` or tests of individual current and end iterators to check whether the loop should continue.  In the last section of the for loop, increment the current-position iterators.
     
     This function differs from `zinit` in that `zinit` returns a tuple of end iterators, designed to be passed to `ztest` but not to be used directly, while this function directly returns individual end iterators.
     
     @param args Containers over which to iterate.  Accepts any container or other object with `begin()` and `end()` members or with `std::begin()` and `std::end()` specializations.  Containers must be lvalues, since an rvalue's lifetime cannot be extended by this function and will expire before the start of the for loop.
     @return A tuple containing one current position iterator per container and one end iterator per container.  Receive these using a structured binding, accepting the elements by value (`auto`) rather than reference (`auto &`).
     */
    template< typename ...Ts >
    auto zinitfull( Ts&... args )
    {
        return std::make_tuple( std::begin( args )..., std::end( args )... );
    }
    
   /**
     For simultaneous iteration over multiple containers in a single conventional for loop, test whether the loop should continue.
     
     This function is designed to be used in the condition section of a conventional for loop, the section which tests whether the loop should continue, when a loop is carried out over multiple containers.  Use it with iterators obtained from `zinit()` or from calls to `zbegin()` and `zend()`.
     
     @param current_iters Tuple of iterators representing the current position within each container.  If `zinit()` was used and the iterators are present as separate variables, they can be passed to this function simply by enclosing them in braces.  For example, if `zinit()` returned two current iterators a and b, pass them to this function as `{ a, b }`.
     @param end_iters Tuple of past-the-end iterators for the containers.  If `zinit()` was used, the last variable in the structured binding will have received this tuple, and it can be passed directly into this function.
     @return `true` if the loop should continue, `false` if it should end.  If the containers are of different lengths, the loop terminates when any one container's iterator reaches that container's end.
     */
    template< typename TupleType >
    bool ztest( const TupleType & current_iters, const TupleType & end_iters )
    {
        return details::comp_neq_tuple_elements( current_iters, end_iters );
    }
    
    /**
     Given a pair of iterators delineating a range of elements from some container, generates a suitable container object to be passed to a range-for loop.
     */
    template< typename IterType >
    auto make_range( IterType begin_iterator, IterType end_iterator )
    {
        return details::range_container< IterType >{ begin_iterator, end_iterator };
    }
    
}

namespace std
{
    template< typename RefTupleType, typename ValueTupleType >
    struct tuple_size< bec::zip_for::details::tuple_return_wrapper< RefTupleType, ValueTupleType > > : std::integral_constant< std::size_t, std::tuple_size_v< RefTupleType > > { };
    
    template< std::size_t I, typename RefTupleType, typename ValueTupleType >
    struct tuple_element< I, bec::zip_for::details::tuple_return_wrapper< RefTupleType, ValueTupleType > >
    {
        using type = decltype( std::declval< bec::zip_for::details::tuple_return_wrapper< RefTupleType, ValueTupleType > >().template get< I >() );
    };
}

#endif
